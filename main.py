#!/usr/bin/env python

import random
import sys

def main():
    num = random.randint(1, 10)
    guesses = 0

    print("I am thinking of a number between 1 and 10, can you guess it?: ")

    while guesses <= 2:
        try:
            guess = int(input())

            if guess != num:
                guesses += 1
                print("That's not the number! Try again: ")
                continue

            elif guess == num:
                print("Congratulations! You guessed the number!")
                break
        except ValueError:
            print("Please enter a number!: ")
            continue


    print("Type \"again\" to play again, or press any key to quit: ")
    ans = str(input())

    if ans == "again":
        main()
    else:
        sys.exit()


if __name__ == "__main__":
    main()
