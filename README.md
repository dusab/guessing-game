# Guessing Game #

This is nothing more than a simple number guessing game. The program generates a random number between 1-10 and give you 3 tries to guess the number.

* Program was written with Python 3 in mind, and uses 
```
#!python

input()
```
instead of 
```
#!python

raw_input()
```
Making the program work for Python 2 would be as simple as switching the 2 previous functions. 